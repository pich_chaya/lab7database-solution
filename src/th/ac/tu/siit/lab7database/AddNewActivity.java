package th.ac.tu.siit.lab7database;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

public class AddNewActivity extends Activity {
	long id;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_new);
		Intent data = getIntent();
		//Check the activity is started with extra
		if (data.hasExtra("id")) {
			EditText etName = (EditText)findViewById(R.id.etName);
			EditText etPhone = (EditText)findViewById(R.id.etPhone);
			EditText etEmail = (EditText)findViewById(R.id.etEmail);
			etName.setText(data.getStringExtra("ct_name"));
			etPhone.setText(data.getStringExtra("ct_phone"));
			etEmail.setText(data.getStringExtra("ct_email"));
			RadioGroup rdg = (RadioGroup)findViewById(R.id.rdgType);
			int iType = data.getIntExtra("ct_type", -1);
			if (iType == R.drawable.home) {
				rdg.check(R.id.rdHome);
			}
			else {
				rdg.check(R.id.rdMobile);
			}
			id = data.getLongExtra("id", -1);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.add_new, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
		case R.id.action_save:
			EditText etName = (EditText)findViewById(R.id.etName);
			EditText etPhone = (EditText)findViewById(R.id.etPhone);
			EditText etEmail = (EditText)findViewById(R.id.etEmail);
			RadioGroup rdg = (RadioGroup)findViewById(R.id.rdgType);
			
			String sName = etName.getText().toString().trim();
			String sPhone = etPhone.getText().toString().trim();
			String sEmail = etEmail.getText().toString().trim();
			int iType = rdg.getCheckedRadioButtonId();
			
			if (sName.length() == 0 || sPhone.length() == 0 || iType == -1) {
				Toast t = Toast.makeText(this, 
						R.string.fields_required, 
						Toast.LENGTH_LONG);
				t.show();
			}
			else {
				//Check if the input email address is valid
				if (sEmail.length() > 0 &&
					!Patterns.EMAIL_ADDRESS.matcher(sEmail).matches()) {
					//Display a message when it is not valid;
					Toast t = Toast.makeText(this, 
							R.string.invalid_email_address, 
							Toast.LENGTH_LONG);
					t.show();
				}
				else {
					Intent data = new Intent();
					data.putExtra("name", sName);
					data.putExtra("phone", sPhone);
					data.putExtra("email", sEmail);
					switch(iType) {
					case R.id.rdHome:
						data.putExtra("type", String.valueOf(R.drawable.home));
						break;
					case R.id.rdMobile:
						data.putExtra("type", String.valueOf(R.drawable.mobile));
						break;
					}
					data.putExtra("id", id);
					setResult(RESULT_OK, data);
					finish();
				}
			}
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	
}
